//
//  Created by Mazen Denden on 8/6/2022.
//

import Foundation

final class NetworkManager: NetworkService {
    static let shared = NetworkManager()
    
    func request(_ request: URLRequest, completionHandler: @escaping @Sendable (Data?, URLResponse?, Error?) -> Void) {
        URLSession.shared
            .dataTask(with: request, completionHandler: completionHandler)
            .resume()
    }
}
