//
//  Created by Mazen Denden on 22/05/2024.
//

import Foundation

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
}

protocol NetworkRouter {
    var baseUrl: String { get }
    var url: URL { get }
    var method: HttpMethod { get }
    var headers: [String: String]? { get }
}
