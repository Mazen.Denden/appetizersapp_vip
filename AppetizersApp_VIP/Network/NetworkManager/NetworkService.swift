//
//  Created by Mazen Denden on 21/6/2022.
//

import Foundation

protocol NetworkService {
    func request(_ request: URLRequest, completionHandler: @escaping @Sendable (Data?, URLResponse?, Error?) -> Void)
}
