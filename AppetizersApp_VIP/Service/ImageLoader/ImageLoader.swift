//
//  ImageLoader.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 27/05/2024.
//

import UIKit

protocol ImageLoader {
    func load(_ imageUrl: URL, completionHandler: @escaping (Result<UIImage, Error>) -> Void)
    func suspend(_ imageUrl: URL)
}

extension ImageLoader {
    func suspend(_ imageUrl: URL) {}
}
