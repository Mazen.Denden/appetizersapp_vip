//
//  DataTaskImageLoader.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 27/05/2024.
//

import UIKit

class DataTaskImageLoader<ImageCache: Cache>: ImageLoader where ImageCache.Key == URL, ImageCache.Object == UIImage {
    private let dataTaskManager = DataTaskManager()
    private let cache: ImageCache?
    
    init(cache: ImageCache?) {
        self.cache = cache
    }
    
    func load(_ imageUrl: URL, completionHandler: @escaping (Result<UIImage, Error>) -> Void) {
        if let cache {
            ///
            /// The image is already cached
            if let cachedImage = cache.getObject(forKey: imageUrl) {
                completionHandler(.success(cachedImage))
                return
            }
        }
        
        Task {
            ///
            /// There is no existing DataTask
            guard let (dataTask, error) = await dataTaskManager.dataTask(for: imageUrl),
                  ///
                  /// There is an Error in the DataTask
                  ///     Replace it with a new one and restart
                  error == nil
            else {
                createAndAddNewDataTask(to: imageUrl, completionHandler: completionHandler)
                return
            }
            
            ///
            /// Resume the existing DataTask
            if case .suspended = dataTask.state {
                await dataTaskManager.resumeTask(imageUrl)
            }
        }
    }
    
    private func createAndAddNewDataTask(to imageUrl: URL, completionHandler: @escaping (Result<UIImage, Error>) -> Void) {
        let dataTask = retrieveImage(imageUrl) { [weak self] result in
            self?.handleResult(result, for: imageUrl, completionHandler: completionHandler)
        }
        
        addTask(dataTask, to: imageUrl)
    }
    
    private func retrieveImage(_ imageUrl: URL, completionHandler: @escaping (Result<UIImage, Error>) -> Void) -> URLSessionDataTask {
        let dataTask = URLSession.shared.dataTask(with: imageUrl) { data, response, error in
            if let error {
                completionHandler(.failure(error))
                return
            }
            
            guard let data else {
                completionHandler(.failure(ImageRetrieveError.missingData))
                return
            }
            
            guard let image = UIImage(data: data) else {
                completionHandler(.failure(ImageRetrieveError.invalidData))
                return
            }
            
            completionHandler(.success(image))
        }
        
        dataTask.resume()
        return dataTask
    }
    
    /// Decorator pattern here
    private func handleResult(_ result: Result<UIImage, Error>, for imageUrl: URL, completionHandler: @escaping (Result<UIImage, Error>) -> Void) {
        switch result {
        case .success(let image):
            completionHandler(.success(image))
            cache?.setObject(image, forKey: imageUrl)
        case .failure(let error):
            completionHandler(.failure(error))
            addErrorToTask(error, for: imageUrl)
        }
    }
    
    private enum ImageRetrieveError: Error {
        case missingData
        case invalidData
    }
    
    private func addTask(_ task: URLSessionTask, to url: URL) {
        Task {
            await dataTaskManager.addTask(task, for: url)
        }
    }
    
    func suspend(_ imageUrl: URL) {
        Task {
            await dataTaskManager.suspend(for: imageUrl)
        }
    }
    
    private func addErrorToTask(_ error: Error, for url: URL) {
        Task {
            await dataTaskManager.addErrorToTask(error, for: url)
        }
    }
}
