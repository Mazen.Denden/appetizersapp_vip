//
//  Created by Mazen Denden on 31/05/2024.
//

import Foundation

class FavoritesUserDefaultsStorage: FavoritesLocalStorage {
    let userDefaults = UserDefaults.standard
    
    func appetizers() -> [String] {
        return getSavedStrings()
    }
    
    func addAppetizer(_ identifier: String) {
        var strings = getSavedStrings()
        
        if !strings.contains(where: { $0 == identifier }) {
            strings.append(identifier)
        }
        
        saveStrings(strings)
    }
    
    func removeAppetizer(_ identifier: String) {
        var strings = getSavedStrings()
        
        guard let index = strings.firstIndex(where: { $0 == identifier }) else {
            Log.error("Failed to remove favorite with identifier: \(identifier)")
            return
        }
        
        strings.remove(at: index)
        saveStrings(strings)
    }
    
    func contains(_ identifier: String) -> Bool {
        return getSavedStrings().contains(where: { $0 == identifier })
    }
    
    private func getSavedStrings() -> [String] {
        guard let savedStrings = userDefaults.array(forKey: "favorites") as? [String]
        else {
            return []
        }
        
        return savedStrings
    }
    
    private func saveStrings(_ strings: [String]) {
        userDefaults.set(strings, forKey: "favorites")
    }
    
}
