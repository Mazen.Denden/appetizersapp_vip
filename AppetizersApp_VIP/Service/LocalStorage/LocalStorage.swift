//
//  Created by Mazen Denden on 31/05/2024.
//

/// Interface Segregation principle
protocol FavoritesLocalStorage: AddFavoritesLocalStorage, FetchFavoritesLocalStorage, ContainsFavoritesLocalStorage {
}

protocol AddFavoritesLocalStorage {
    func addAppetizer(_ identifier: String)
    func removeAppetizer(_ identifier: String)
}

protocol FetchFavoritesLocalStorage {
    func appetizers() -> [String]
}

protocol ContainsFavoritesLocalStorage {
    func contains(_ identifier: String) -> Bool
}
