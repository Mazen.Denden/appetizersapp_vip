//
//  Created by Mazen Denden on 04/06/2024.
//

protocol Cache {
    associatedtype Key: Hashable
    associatedtype Object
    
    func getObject(forKey key: Key) -> Object?
    func setObject(_ object: Object, forKey key: Key)
    func removeObject(forKey key: Key)
    func clear()
}
