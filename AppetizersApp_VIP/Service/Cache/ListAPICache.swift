//
//  Created by Mazen Denden on 10/06/2024.
//

import Foundation

struct ListAPICache: APIResponseCache {
    func cacheResponse(for request: URLRequest, _ data: Data, _ response: URLResponse?) {
        if let response {
            let cachedResponse = CachedURLResponse(response: response, data: data)
            URLCache.shared.storeCachedResponse(cachedResponse, for: request)
        }
    }
}
