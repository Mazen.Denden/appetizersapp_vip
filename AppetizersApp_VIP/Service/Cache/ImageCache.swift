//
//  Created by Mazen Denden on 27/05/2024.
//

import UIKit

class ImageCache<Key: Hashable, Object>: Cache {
    private let cache = NSCache<NSURL, UIImage>()

    func getObject(forKey key: URL) -> UIImage? {
        return cache.object(forKey: key as NSURL)
    }
    
    func setObject(_ object: UIImage, forKey key: URL) {
        cache.setObject(object, forKey: key as NSURL)
    }
    
    func removeObject(forKey key: URL) {
        cache.removeObject(forKey: key as NSURL)
    }
    
    func clear() {
        cache.removeAllObjects()
    }
    
}
