//
//  Created by Mazen Denden on 10/06/2024.
//

import Foundation

protocol APIResponseCache {
    func cacheResponse(for request: URLRequest, _ data: Data, _ response: URLResponse?)
}
