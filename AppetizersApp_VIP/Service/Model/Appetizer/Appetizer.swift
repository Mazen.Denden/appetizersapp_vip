//
//  Created by Mazen Denden on 8/6/2022.
//

import Foundation

struct Appetizer: Codable {
    let name: String
    let price: Double
    let imageURL: String
    let description: String
}

extension Appetizer {
    static let samples: [Appetizer] = [sampleAppetizer, sampleAppetizer, sampleAppetizer]
    static let sampleAppetizer = Appetizer(name: "Blackened Shrimp",
                                           price: 6.99, 
                                           imageURL: "https://seanallen-course-backend.herokuapp.com/images/appetizers/blackened-shrimp.jpg",
                                           description: "Seasoned shrimp from the depths of the Atlantic Ocean.")
}
