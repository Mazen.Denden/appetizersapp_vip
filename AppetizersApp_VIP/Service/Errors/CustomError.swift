//
//  Created by Mazen Denden on 04/06/2024.
//

import Foundation

enum CustomError: Error {
    case networkError(_ error: Error)
}

extension CustomError: Equatable {
    static func == (lhs: CustomError, rhs: CustomError) -> Bool {
        if case .networkError(let lhsError) = lhs, case .networkError(let rhsError) = rhs {
            return (lhsError as NSError).domain == (rhsError as NSError).domain && (lhsError as NSError).code == (rhsError as NSError).code
        }
        return false
    }
    
}
