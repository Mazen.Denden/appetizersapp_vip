//
//  Created by Mazen Denden on 22/05/2024.
//

import Foundation

enum ListServiceResponseError: Error, Equatable {
    case noResponse
    case invalidResponse
    case noData
}

enum ListServiceDecodingError: Error, Equatable {
    case decodingError
}
