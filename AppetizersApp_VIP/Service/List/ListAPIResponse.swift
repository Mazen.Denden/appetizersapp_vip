//
//  Created by Mazen Denden on 04/06/2024.
//

import Foundation

protocol ListAPIResponse {
    func handleResponse(data: Data?, response: URLResponse?, error: Error?) throws -> Data
}

class ListAPIResponseService: ListAPIResponse {
    func handleResponse(data: Data?, response: URLResponse?, error: Error?) throws -> Data {
        if let error {
            throw CustomError.networkError(error)
        }
        
        guard let response else {
            throw ListServiceResponseError.noResponse
        }
        
        let httpResponse = response as! HTTPURLResponse
        guard 200...299 ~= httpResponse.statusCode else {
            throw ListServiceResponseError.invalidResponse
        }
        
        guard let data else {
            throw ListServiceResponseError.noData
        }
        
        return data
    }
}
