//
//  Created by Mazen Denden on 21/6/2022.
//

struct AppetizerResponse: Decodable {
    let appetizers: [Appetizer]
    
    private enum CodingKeys: String, CodingKey {
        case appetizers = "request"
    }
}
