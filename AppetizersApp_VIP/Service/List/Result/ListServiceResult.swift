//
//  Created by Mazen Denden on 22/05/2024.
//

enum ListServiceResult {
    case success([Appetizer])
    case failure(error: Error)
}
