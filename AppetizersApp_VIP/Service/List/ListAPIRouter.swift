//
//  Created by Mazen Denden on 22/05/2024.
//

import Foundation

struct ListAPIRouter {
    let route: Route
    
    enum Route {
        case fetch
    }
    
    var urlRequest: URLRequest {
        switch route {
        case .fetch:
            setupFetchURLCache()
            var urlRequest = urlRequest(for: url, httpMethod: method.rawValue)
            urlRequest.cachePolicy = .returnCacheDataElseLoad
            return urlRequest
        }
    }
    
    private func setupFetchURLCache() {
        let memoryCapacity = 4 * 1024 * 1024
        let diskCapacity = 100 * 1024 * 1024
        let urlCache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "fetchCache")
        URLCache.shared = urlCache
    }
    
    private func urlRequest(for url: URL, httpMethod: String) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }
}

extension ListAPIRouter: NetworkRouter {
    var baseUrl: String {
        return ServiceConstants.List.baseURL
    }
    
    var url: URL {
        switch route {
        case .fetch:
            return URL(string: baseUrl + "/appetizers")!
        }
    }
    
    var method: HttpMethod {
        switch route {
        case .fetch:
            return .get
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
    
}
