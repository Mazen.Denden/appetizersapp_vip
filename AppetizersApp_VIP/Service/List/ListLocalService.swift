//
//  ListLocalService.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 31/05/2024.
//

import Foundation

class ListLocalService: ListService {
    
    var localStorage: FetchFavoritesLocalStorage?
    
    // Decorator pattern
    private let apiService: ListService
    
    init(apiService: ListService) {
        self.apiService = apiService
    }
    
    func fetchAppetizers(completion: @escaping (ListServiceResult) -> Void) {
        apiService.fetchAppetizers { [weak self] result in
            switch result {
            case .success(let appetizers):
                guard let localStorage = self?.localStorage
                else {
                    completion(.failure(error: ListLocalServiceError()))
                    return
                }
                
                let favoriteIdentifiers = localStorage.appetizers()
                let favorites = appetizers.filter {
                    favoriteIdentifiers.contains($0.name)
                }
                completion(.success(favorites))
            case .failure(let error):
                completion(.failure(error: error))
            }
        }
    }
}

extension ListLocalService {
    struct ListLocalServiceError: Error {
    }
}
