//
//  Created by Mazen Denden on 20/6/2022.
//

import Foundation

protocol ListService {
    func fetchAppetizers(completion: @escaping (ListServiceResult) -> Void)
}

protocol Decoder {
    func decode<T>(_ data: Data) throws -> T where T: Decodable
}

struct ServiceDecoder: Decoder {
    let decoder = JSONDecoder()
    
    func decode<T>(_ data: Data) throws -> T where T: Decodable {
        return try decoder.decode(T.self, from: data)
    }
}

class ListAPIService: ListService {
    private let networkService: NetworkService
    private let responseService: ListAPIResponse
    private let decoder: Decoder
    
    var apiCache: APIResponseCache?
    
    init(networkService: NetworkService = NetworkManager.shared,
         responseService: ListAPIResponse,
         decoder: Decoder) {
        
        self.networkService = networkService
        self.responseService = responseService
        self.decoder = decoder
    }
    
    func fetchAppetizers(completion: @escaping (ListServiceResult) -> Void) {
        let request = ListAPIRouter(
            route: .fetch
        ).urlRequest
        
        networkService.request(request) { [self] data, response, error in
            do {
                let data = try self.responseService.handleResponse(data: data, response: response, error: error)
                let decodedResponse: AppetizerResponse = try self.decoder.decode(data)
                
                apiCache?.cacheResponse(for: request, data, response)
                
                completion(.success(decodedResponse.appetizers))
            } catch let listServiceResponseError as ListServiceResponseError {
                completion(.failure(error: listServiceResponseError))
            } catch let listServiceDecodingError as ListServiceDecodingError {
                completion(.failure(error: listServiceDecodingError))
            } catch {
                completion(.failure(error: error))
            }
        }
    }
    
}
