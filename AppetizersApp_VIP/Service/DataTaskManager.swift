//
//  DataTaskManager.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 27/05/2024.
//

import Foundation

actor DataTaskManager {
    private var dataTasks: [URL: (URLSessionTask, Error?)] = [:]
    
    func addTask(_ task: URLSessionTask, for url: URL) {
        dataTasks[url] = (task, nil)
    }
    
    func dataTask(for url: URL) -> (URLSessionTask, Error?)? {
        return dataTasks.first(where: { $0.key == url })?.value
    }
    
    func resumeTask(_ url: URL) {
        guard let (dataTask, _) = dataTasks.first(where: { $0.key == url })?.value else {
            return
        }
        
        dataTask.resume()
    }
    
    func suspend(for url: URL) {
        guard let (dataTask, _) = dataTasks.first(where: { $0.key == url })?.value else {
            return
        }
        
        if dataTask.state == .running {
            dataTask.suspend()
        }
    }
    
    func addErrorToTask(_ error: Error, for url: URL) {
        guard let (dataTask, _) = dataTasks.first(where: { $0.key == url })?.value else {
            return
        }
        
        dataTasks[url] = (dataTask, error)
    }
    
}
