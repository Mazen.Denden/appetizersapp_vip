//
//  Created by Mazen Denden on 14/05/2024.
//

import UIKit

protocol SceneFactory {
    func makeList(selectionCallback: @escaping (AppetizerViewModel) -> Void) -> UIViewController
    func makeAppetizerDetails(with item: AppetizerViewModel) -> UIViewController
    func makeFavorites() -> UIViewController
}
