//
//  Created by Mazen Denden on 14/05/2024.
//

import SwiftUI

class SwiftUISceneFactory: SceneFactory {
    
    let imageDataSource: ImageLoader
    
    init() {
        let imageCache = ImageCache<NSURL, UIImage>()
        imageDataSource = DataTaskImageLoader(cache: imageCache)
    }
    
    func makeList(selectionCallback: @escaping (AppetizerViewModel) -> Void) -> UIViewController {
        let viewController: ListViewController = ListViewController.instantiateInitial(from: StoryboardId.list)
        viewController.selection = selectionCallback
        
        let configurator = ListConfigurator(imageLoader: imageDataSource)
        configurator.configure(viewController)
        
        return viewController
    }
    
    func makeAppetizerDetails(with appetizerViewModel: AppetizerViewModel) -> UIViewController {
        let configurator = DetailsViewConfigurator(appetizerViewModel: appetizerViewModel, imageLoader: imageDataSource)
        let detailsView = configurator.configure()
        
        return UIHostingController(rootView: detailsView)
    }
    
    func makeFavorites() -> UIViewController {
        let viewController: ListViewController = ListViewController.instantiateInitial(from: StoryboardId.list)
        
        let configurator = FavoritesConfigurator(imageLoader: imageDataSource)
        configurator.configure(viewController)
        
        return viewController
    }
    
}
