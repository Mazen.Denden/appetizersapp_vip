//
//  Created by Mazen Denden on 04/06/2024.
//

import Foundation

protocol TestableDispatchQueue {
    func async(execute work: @escaping @convention(block) () -> Void)
}

extension DispatchQueue: TestableDispatchQueue {
    func async(execute work: @escaping @convention(block) () -> Void) {
        async(group: nil, qos: .unspecified, flags: [], execute: work)
    }
}
