//
//  ImageConstants.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 04/06/2024.
//

import UIKit

extension UIImage {
    enum SF {
        static let star = SFImageAsset(name: "star")
        static let starFill = SFImageAsset(name: "star.fill")
        
        static let house = SFImageAsset(name: "house")
        static let houseFill = SFImageAsset(name: "house.fill")
    }
}

struct SFImageAsset {
    let name: String

    var uiImage: UIImage {
        guard let result = UIImage(systemName: name) else {
            fatalError("Unable to load systemName named \(name).")
        }
        return result
    }
}
