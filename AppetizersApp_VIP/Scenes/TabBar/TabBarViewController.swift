//
//  Created by Mazen Denden on 24/6/2022.
//

import UIKit

enum TabType {
    case home
    case favorites
}

class TabBarViewController: UITabBarController {
    
    var factory: SceneFactory!
    var router: NavigationRouter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllers(makeTabs(), animated: false)
    }
    
    private func makeTabs() -> [UIViewController] {
        [TabType.home, .favorites].map { tab in
            switch tab {
            case .home:
                return makeListViewController()
            case .favorites:
                return makeFavoritesViewController()
            }
        }
    }
    
    private func makeListViewController() -> UIViewController {
        let tabItem = UITabBarItem(title: "Home",
                                   image: .SF.house.uiImage,
                                   selectedImage: .SF.house.uiImage)
        
        let viewController = factory.makeList { [self] viewModel in
            router.show(self.factory.makeAppetizerDetails(with: viewModel), on: .home)
        }
        viewController.tabBarItem = tabItem
        
        let navigationController = UINavigationController(rootViewController: viewController)
        router.createNavigation(
            for: .home,
            with: navigationController
        )
        
        return navigationController
    }
    
    private func makeFavoritesViewController() -> UIViewController {
        let tabItem = UITabBarItem(title: "Favorites",
                                   image: .SF.star.uiImage,
                                   selectedImage: .SF.starFill.uiImage)
        
        let viewController = factory.makeFavorites()
        viewController.tabBarItem = tabItem
        
        let navigationController = UINavigationController(rootViewController: viewController)
        router.createNavigation(
            for: .favorites,
            with: navigationController
        )
        
        return navigationController
    }
    
}
