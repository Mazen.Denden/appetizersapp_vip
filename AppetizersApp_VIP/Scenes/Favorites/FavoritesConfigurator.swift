//
//  Created by Mazen Denden on 31/05/2024.
//

final class FavoritesConfigurator {
    
    let imageLoader: ImageLoader
    init(imageLoader: ImageLoader) {
        self.imageLoader = imageLoader
    }
    
    func configure(_ viewController: ListViewController) {
        let interactor = ListInteractorImplementation()
        let presenter = ListPresenterImplementation()
        
        let localStorage = FavoritesUserDefaultsStorage()
        let apiService = ListAPIService(
            responseService: ListAPIResponseService(),
            decoder: ServiceDecoder()
        )
        let listService = ListLocalService(
            apiService: apiService
        )
        listService.localStorage = localStorage
        
        let listWorker = ListWorkerImplementation(
            listService: listService
        )
        let listLocalStorageWorker = ListStorageWorker(
            localStorage: localStorage
        )
        
        let dataStore = ListDataStore()
        let viewModel = FavoritesViewModel(
            dataStore: dataStore, 
            localStorage: localStorage
        )
        viewModel.config = ListConfig(numberOfShimmerCells: 3)
        
        viewController.interactor = interactor
        viewController.listViewModel = viewModel
        viewController.imageDataSource = imageLoader
        viewController.onFavorite = { [weak localStorage] (identifier, isFavorite) in
            if isFavorite {
                localStorage?.addAppetizer(identifier)
            } else {
                localStorage?.removeAppetizer(identifier)
            }
            
            /// This is the easiest way to have the correct data,
            /// we can call a viewcontroller function that starts a vip cycle to remove a certain cell at a certain index
            interactor.fetchAppetizers()
        }
        
        interactor.presenter = presenter
        interactor.listWorker = listWorker
        interactor.dataStore = dataStore
        interactor.listStorageWorker = listLocalStorageWorker
        
        presenter.viewController = viewController
    }
}
