//
//  Created by Mazen Denden on 31/05/2024.
//

import UIKit

final class FavoritesViewModel: ListViewModel {
    
    override var title: String {
        return "Favorites"
    }
    
    override var emptyStateConfig: UIContentUnavailableConfiguration {
        var config = UIContentUnavailableConfiguration.empty()
        config.image = .SF.starFill.uiImage
        config.text = "No Favorites"
        config.secondaryText = "Your favorite Appetizers will appear here."
        return config
    }
}
