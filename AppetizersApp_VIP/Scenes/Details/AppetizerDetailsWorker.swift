//
//  Created by Mazen Denden on 04/11/2022.
//

import UIKit

class AppetizerDetailsWorker {
    
    private let imageDataSource: ImageLoader
    
    init(imageDataSource: ImageLoader) {
        self.imageDataSource = imageDataSource
    }
    
    func fetchImage(with url: URL, completion: @escaping (UIImage?) -> Void) {
        imageDataSource.load(url) { result in
            switch result {
            case .success(let image):
                completion(image)
            case .failure:
                completion(nil)
            }
        }
    }
}
