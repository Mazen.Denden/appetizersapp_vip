//
//  Created by Mazen Denden on 31/05/2024.
//

class AppetizerDetailsLocalStorageWorker {
    let localStorage: (AddFavoritesLocalStorage & ContainsFavoritesLocalStorage)

    init(localStorage: AddFavoritesLocalStorage & ContainsFavoritesLocalStorage) {
        self.localStorage = localStorage
    }
    
    func save(_ identifier: String) {
        localStorage.addAppetizer(identifier)
    }
    
    func remove(_ identifier: String) {
        localStorage.removeAppetizer(identifier)
    }
    
    func appetizerIsFavorite(with identifier: String) -> Bool {
        localStorage.contains(identifier)
    }
}
