//
//  Created by Mazen Denden on 04/11/2022.
//

final class AppetizerDetailsConfigurator {
    let item: AppetizerViewModel
    let imageLoader: ImageLoader
    
    init(item: AppetizerViewModel, imageLoader: ImageLoader) {
        self.item = item
        self.imageLoader = imageLoader
    }
    
    func configure(_ viewController: AppetizerDetailsViewController) {
        let interactor = AppetizerDetailsInteractorImplementation()
        let localStorage = FavoritesUserDefaultsStorage()
        let presenter = AppetizerDetailsPresenterImplementation()
        
        let worker = AppetizerDetailsWorker(
            imageDataSource: imageLoader
        )
        let localStorageWorker = AppetizerDetailsLocalStorageWorker(
            localStorage: localStorage
        )
        
        viewController.interactor = interactor
        viewController.viewModel = item
        
        interactor.presenter = presenter
        interactor.worker = worker
        interactor.localStorageWorker = localStorageWorker
        
        presenter.viewController = viewController
    }
}
