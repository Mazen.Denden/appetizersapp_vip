//
//  Created by Mazen Denden on 04/11/2022.
//

import Foundation

protocol AppetizerDetailsInteractor {
    func fetchImage(with url: URL)
    func fetchIsFavorite(with identifier: String)
    func updateAppetizerLocalStorage(with identifier: String, isFavorite: Bool)
}

class AppetizerDetailsInteractorImplementation: AppetizerDetailsInteractor {
    var presenter: AppetizerDetailsPresenter?
    var worker: AppetizerDetailsWorker?
    var localStorageWorker: AppetizerDetailsLocalStorageWorker?
    
    func fetchImage(with url: URL) {
        worker?.fetchImage(with: url) { [weak self] image in
            guard let image
            else {
                self?.presenter?.presentError()
                return
            }
            
            self?.presenter?.present(image: image)
        }
    }
    
    func fetchIsFavorite(with identifier: String) {
        let isFavorite = localStorageWorker?.appetizerIsFavorite(with: identifier) ?? false
        presenter?.presentFavorite(isFavorite)
    }
    
    func updateAppetizerLocalStorage(with identifier: String, isFavorite: Bool) {
        if isFavorite {
            saveAppetizerToLocal(with: identifier)
        } else {
            removeAppetizerFromLocal(with: identifier)
        }
    }
    
    private func saveAppetizerToLocal(with identifier: String) {
        localStorageWorker?.save(identifier)
    }
    
    private func removeAppetizerFromLocal(with identifier: String) {
        localStorageWorker?.remove(identifier)
    }
}
