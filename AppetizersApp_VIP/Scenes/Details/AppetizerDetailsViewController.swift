//
//  Created by Mazen Denden on 28/10/2022.
//

import UIKit

protocol AppetierDetailsViewControllerOutput {
    func display(image: UIImage)
    func displayAlert()
    func displayFavorite(_ isFavorite: Bool)
}

class AppetizerDetailsViewController: UIViewController {
    
    var interactor: AppetizerDetailsInteractor?
    var viewModel: AppetizerViewModel!
    
    @IBOutlet weak private var descriptionLabel: UILabel!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var appetizerImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupViewModel()
    }
    
    private func setupViews() {
        setupNavBar()
    }
    
    private func setupNavBar() {
        let addToFavoriteBarButtonItem = UIBarButtonItem(image: .SF.star.uiImage,
                                                         style: .plain,
                                                         target: self,
                                                         action: #selector(handleAddToFavorite))
        addToFavoriteBarButtonItem.tintColor = .systemYellow
        self.navigationItem.rightBarButtonItem = addToFavoriteBarButtonItem
    }
    
    @objc private func handleAddToFavorite(_ sender: Any) {
        viewModel.toggleFavorite()
    }
    
    private func setupRightBarButtonIcon(isFavorite: Bool) {
        navigationItem.rightBarButtonItem?.image = isFavorite ? UIImage.SF.starFill.uiImage: .SF.star.uiImage
    }
    
    private func setupViewModel() {
        nameLabel.text = viewModel.name
        priceLabel.text = viewModel.price
        descriptionLabel.text = viewModel.description
                
        interactor?.fetchIsFavorite(with: viewModel.identifier)
        interactor?.fetchImage(with: viewModel.imageURL)
        
        viewModel.onFavorite = { [weak self] isFavorite in
            guard let self else { return }
            self.setupRightBarButtonIcon(isFavorite: isFavorite)
            self.interactor?.updateAppetizerLocalStorage(with: self.viewModel.identifier, isFavorite: isFavorite)
        }
    }
    
}

extension AppetizerDetailsViewController: AppetierDetailsViewControllerOutput {
    func display(image: UIImage) {
        appetizerImageView.image = image
    }
    
    func displayAlert() {
        showAlert(title: "",
                  message: "Failed to retrieve image", 
                  buttonTitle: "Ok")
    }
    
    func displayFavorite(_ isFavorite: Bool) {
        setupRightBarButtonIcon(isFavorite: isFavorite)
    }
    
}
