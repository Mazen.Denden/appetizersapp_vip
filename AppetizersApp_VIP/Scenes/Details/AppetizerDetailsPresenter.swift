//
//  Created by Mazen Denden on 04/11/2022.
//

import UIKit

protocol AppetizerDetailsPresenter {
    func present(image: UIImage)
    func presentError()
    func presentFavorite(_ isFavorite: Bool)
}

class AppetizerDetailsPresenterImplementation: AppetizerDetailsPresenter {
    var viewController: AppetierDetailsViewControllerOutput?
    
    func present(image: UIImage) {
        DispatchQueue.main.async {
            self.viewController?.display(image: image)
        }
    }
    
    func presentError() {
        DispatchQueue.main.async {
            self.viewController?.displayAlert()
        }
    }
    
    func presentFavorite(_ isFavorite: Bool) {
        DispatchQueue.main.async {
            self.viewController?.displayFavorite(isFavorite)
        }
    }
}
