//
//  DetailsViewModel.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 25/06/2024.
//

import Foundation

class DetailsViewModel {
    let description: String
    let name: String
    let price: String
    let onFavorite: (Bool) -> Void
    
    init(description: String, name: String, price: String, onFavorite: @escaping (Bool) -> Void) {
        self.description = description
        self.name = name
        self.price = price
        self.onFavorite = onFavorite
    }
}
