//
//  DetailsView.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 24/06/2024.
//

import SwiftUI

struct DetailsView: View {
    
    let viewModel: DetailsViewModel
    @StateObject var detailsObservable: DetailsObservableObject
    
    var body: some View {
        ScrollView {
            VStack {
                ZStack(alignment: .bottomTrailing) {
                    Color.gray
                        .opacity(0.1)
                    
                    if let uiImage = detailsObservable.image {
                        Image(uiImage: uiImage)
                            .resizable()
                            .scaledToFill()
                            .clipped()
                    }
                    
                    Button {
                        viewModel.onFavorite(!detailsObservable.isFavorite)
                    } label: {
                        Image(systemName: detailsObservable.isFavorite ? "star.fill" : "star")
                            .foregroundStyle(.yellow)
                            .font(.title3)
                    }
                    .padding()
                }
                .frame(height: 250)
                
                VStack(alignment: .leading, spacing: 8) {
                    HStack {
                        Text(viewModel.name)
                            .font(.headline)
                        
                        Spacer()
                        
                        Text(viewModel.price)
                            .font(.callout)
                    }
                    
                    Text(viewModel.description)
                        .font(.callout)
                        .foregroundStyle(Color.gray)
                    
                }
                .padding()
                
                Spacer()
            }
        }
        .ignoresSafeArea(edges: .top)
        .onAppear {
            detailsObservable.fetchDetails()
        }
    }
}

#Preview {
    let sample = Appetizer.sampleAppetizer
    let viewModel = DetailsViewModel(description: sample.description,
                                     name: sample.name,
                                     price: "$\(sample.price)",
                                     onFavorite: {
        _ in
    })
    
    let detailsObservable = DetailsObservableObject()
    detailsObservable.appetizerViewModel = .init(appetizer: sample, isFavorite: false)
    
    return DetailsView(viewModel: viewModel, detailsObservable: detailsObservable)
}
