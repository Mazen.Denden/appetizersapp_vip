//
//  DetailsObservableObject.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 25/06/2024.
//

import UIKit

class DetailsObservableObject: ObservableObject {
    @Published var image: UIImage?
    @Published var isFavorite: Bool = false
    
    var interactor: AppetizerDetailsInteractor?
    var appetizerViewModel: AppetizerViewModel!
    
    func fetchDetails() {
        interactor?.fetchIsFavorite(with: appetizerViewModel.identifier)
        interactor?.fetchImage(with: appetizerViewModel.imageURL)
    }
}

extension DetailsObservableObject: AppetierDetailsViewControllerOutput {
    func display(image: UIImage) {
        self.image = image
    }
    
    func displayAlert() {
        print("displayAlert")
    }
    
    func displayFavorite(_ isFavorite: Bool) {
        self.isFavorite = isFavorite
    }
    
}
