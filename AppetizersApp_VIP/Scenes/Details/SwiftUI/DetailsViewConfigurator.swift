//
//  Created by Mazen Denden on 25/06/2024.
//

class DetailsViewConfigurator {
    let appetizerViewModel: AppetizerViewModel
    let imageLoader: ImageLoader
    
    init(appetizerViewModel: AppetizerViewModel, imageLoader: ImageLoader) {
        self.appetizerViewModel = appetizerViewModel
        self.imageLoader = imageLoader
    }
    
    func configure() -> DetailsView {
        let interactor = AppetizerDetailsInteractorImplementation()
        let localStorage = FavoritesUserDefaultsStorage()
        let presenter = AppetizerDetailsPresenterImplementation()
        
        let worker = AppetizerDetailsWorker(
            imageDataSource: imageLoader
        )
        let localStorageWorker = AppetizerDetailsLocalStorageWorker(
            localStorage: localStorage
        )
        
        let detailsObservable = DetailsObservableObject()
        detailsObservable.interactor = interactor
        detailsObservable.appetizerViewModel = appetizerViewModel
        
        interactor.presenter = presenter
        interactor.worker = worker
        interactor.localStorageWorker = localStorageWorker
        
        presenter.viewController = detailsObservable
        
        appetizerViewModel.onFavorite = { [appetizerViewModel] _ in
            interactor.updateAppetizerLocalStorage(
                with: appetizerViewModel.identifier,
                isFavorite: !localStorageWorker.appetizerIsFavorite(with: appetizerViewModel.identifier)
            )
            interactor.fetchIsFavorite(with: appetizerViewModel.identifier)
        }
        
        let viewModel = DetailsViewModel(
            description: appetizerViewModel.description,
            name: appetizerViewModel.name,
            price: appetizerViewModel.price,
            onFavorite: appetizerViewModel.onFavorite!)
        
        let detailsView = DetailsView(viewModel: viewModel, detailsObservable: detailsObservable)
        
        return detailsView
    }
}
