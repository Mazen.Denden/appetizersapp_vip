//
//  AppetizerCell.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import UIKit

final class AppetizerCell: UITableViewCell {
    static let reuseIdentifier: String = "AppetizerCell"
    
    @IBOutlet weak var appetizerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak private var retryButton: UIButton!
    
    var appetizerViewModel: AppetizerViewModel? {
        didSet {
            guard let viewModel = appetizerViewModel else { return }
            setupViewModel(viewModel: viewModel)
            setupBindings(viewModel: viewModel)
        }
    }
    
    var onFavorite: ((String, Bool) -> Void)?
    var retry: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        appetizerImageView.layer.cornerRadius = 10
        appetizerImageView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        appetizerImageView.image = nil
    }
    
    @IBAction private func favoriteAction(_ sender: Any) {
        guard let viewModel = appetizerViewModel
        else {
            return
        }
        
        viewModel.toggleFavorite()
    }
    
    @IBAction private func retryAction(_ sender: Any) {
        setRetryButtonVisibility(true)
        retry?()
    }
    
    func setImage(_ image: UIImage) {
        setRetryButtonVisibility(true)
        appetizerImageView.image = image
    }
    
    func setError() {
        setRetryButtonVisibility(false)
    }
    
    private func setRetryButtonVisibility(_ isHidden: Bool) {
        retryButton.isHidden = isHidden
    }
    
}

// MARK: - Setup Functions
extension AppetizerCell {
    private func setupViewModel(viewModel: AppetizerViewModel) {
        nameLabel.text = viewModel.name
        priceLabel.text = viewModel.price
        setFavoriteIcon(viewModel.isFavorite)
    }
    
    private func setupBindings(viewModel: AppetizerViewModel) {
        viewModel.onFavorite = { [weak self] isFavorite in
            self?.setFavoriteIcon(isFavorite)
            self?.onFavorite?(viewModel.identifier, isFavorite)
        }
    }
    
    private func setFavoriteIcon(_ isFavorite: Bool) {
        favoriteButton.setImage(isFavorite ? .SF.starFill.uiImage : .SF.star.uiImage, for: .normal)
    }
}
