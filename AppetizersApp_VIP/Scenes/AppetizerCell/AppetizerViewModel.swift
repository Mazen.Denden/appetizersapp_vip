//
//  AppetizerViewModel.swift
//  AppetizersAppUIKit
//
//  Created by Mazen Denden on 8/6/2022.
//

import Foundation

final class AppetizerViewModel {
    
    private let appetizer: Appetizer
    private(set) var isFavorite: Bool = false
    
    var onFavorite: ((Bool) -> Void)?
    
    init(appetizer: Appetizer, isFavorite: Bool) {
        self.appetizer = appetizer
        self.isFavorite = isFavorite
    }
    
    var identifier: String { return appetizer.name }
    var name: String { return appetizer.name }
    var imageURL: URL { return URL(string: appetizer.imageURL)! }
    var price: String { return "$\(String(format:"%.2f", appetizer.price))" }
    var description: String { return appetizer.description }
    
    @objc func toggleFavorite() {
        isFavorite.toggle()
        onFavorite?(isFavorite)
    }
}
