//
//  Created by Mazen Denden on 28/10/2022.
//

import UIKit

protocol ListViewControllerOutput {
    func display(_ appetizers: [AppetizerViewModel])
    func displayLoader(_ isLoading: Bool)
    func displayDetails(for item: AppetizerViewModel)
    func displayEmptyState(_ isEmpty: Bool)
    func displayAlert()
}

class ListViewController: UITableViewController {
    var listViewModel: ListViewProtocol!
    
    var interactor: ListInteractor?
    var imageDataSource: ImageLoader?
    var onFavorite: ((String, Bool) -> Void)?
    
    var selection: ((AppetizerViewModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupTableView()
    }
    
    override func viewIsAppearing(_ animated: Bool) {
        super.viewIsAppearing(animated)
        interactor?.fetchAppetizers()
    }
    
    private func setupViews() {
        navigationItem.title = listViewModel.title
        navigationItem.backButtonTitle = listViewModel.backButtonTitle
    }
    
    private func setupTableView() {
        tableView.register(
            UINib(nibName: AppetizerCell.reuseIdentifier, bundle: nil),
            forCellReuseIdentifier: AppetizerCell.reuseIdentifier)
    }
}

extension ListViewController: ListViewControllerOutput {
    func display(_ appetizers: [AppetizerViewModel]) {
        displayEmptyState(appetizers.isEmpty)
        displayLoader(false)
    }
    
    func displayLoader(_ isLoading: Bool) {
        listViewModel.isLoading = isLoading
        tableView.reloadData()
    }
    
    func displayDetails(for item: AppetizerViewModel) {
        selection?(item)
    }
    
    func displayEmptyState(_ isEmpty: Bool) {
        contentUnavailableConfiguration = isEmpty ? listViewModel.emptyStateConfig : nil
    }
    
    func displayAlert() {
        self.showAlert(
            title: "",
            message: "Failed to show appetizers",
            buttonTitle: "Refresh") { [weak self] _ in
                self?.interactor?.fetchAppetizers()
            }
    }
    
}

extension ListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listViewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AppetizerCell.reuseIdentifier, for: indexPath) as? AppetizerCell
        else {
            return UITableViewCell()
        }
        
        let appetizerViewModel = listViewModel.appetizer(at: indexPath.row)
        cell.appetizerViewModel = appetizerViewModel
        cell.onFavorite = onFavorite
        
        if let imageURL = appetizerViewModel?.imageURL {
            loadImage(imageURL, at: indexPath)
            
            cell.retry = { [weak self] in
                self?.loadImage(imageURL, at: indexPath)
            }
        }
        
        return cell
    }
    
    private func loadImage(_ imageUrl: URL, at indexPath: IndexPath) {
        imageDataSource?.load(imageUrl) { [weak self] result in
            DispatchQueue.main.async {
                guard let cell = self?.cellAt(indexPath) else {
                    return
                }
                
                switch result {
                    case .success(let image):
                        cell.setImage(image)
                    case .failure:
                        cell.setError()
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor?.didSelectItem(at: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /// Shimmer
        cell.setTemplateWithSubviews(listViewModel.isLoading ?? false, animate: true, viewBackgroundColor: .systemBackground)
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let appetizerViewModel = listViewModel.appetizer(at: indexPath.row)
        else {
            return
        }
        
        imageDataSource?.suspend(appetizerViewModel.imageURL)
    }
    
    private func cellAt(_ indexPath: IndexPath) -> AppetizerCell? {
        return tableView.cellForRow(at: indexPath) as? AppetizerCell
    }
    
}
