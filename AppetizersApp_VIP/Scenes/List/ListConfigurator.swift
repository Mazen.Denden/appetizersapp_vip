//
//  Created by Mazen Denden on 03/11/2022.
//

final class ListConfigurator {
    
    let imageLoader: ImageLoader
    init(imageLoader: ImageLoader) {
        self.imageLoader = imageLoader
    }
    
    func configure(_ viewController: ListViewController) {
        let interactor = ListInteractorImplementation()
        let presenter = ListPresenterImplementation()
        let listService = ListAPIService(
            responseService: ListAPIResponseService(),
            decoder: ServiceDecoder()
        )
        listService.apiCache = ListAPICache()
        let listWorker = ListWorkerImplementation(
            listService: listService
        )
        let localStorage = FavoritesUserDefaultsStorage()
        let listLocalStorageWorker = ListStorageWorker(
            localStorage: localStorage
        )
        let dataStore = ListDataStore()
        let viewModel = ListViewModel(
            dataStore: dataStore, 
            localStorage: localStorage
        )
        viewModel.config = ListConfig(numberOfShimmerCells: 5)
        
        viewController.interactor = interactor
        viewController.listViewModel = viewModel
        viewController.imageDataSource = imageLoader
        viewController.onFavorite = { [weak localStorage] (identifier, isFavorite) in
            if isFavorite {
                localStorage?.addAppetizer(identifier)
            } else {
                localStorage?.removeAppetizer(identifier)
            }
        }
        
        interactor.presenter = presenter
        interactor.listWorker = listWorker
        interactor.dataStore = dataStore
        interactor.listStorageWorker = listLocalStorageWorker
        
        presenter.viewController = viewController
    }
}
