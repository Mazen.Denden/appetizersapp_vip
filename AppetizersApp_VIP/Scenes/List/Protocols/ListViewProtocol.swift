//
//  ListProtocol.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 31/05/2024.
//

import UIKit

protocol ListViewProtocol {
    var title: String { get }
    var backButtonTitle: String { get }
    var isLoading: Bool? { get set }
    var numberOfRows: Int { get }
    var emptyStateConfig: UIContentUnavailableConfiguration { get }
    
    func appetizer(at index: Int) -> AppetizerViewModel?
}
