//
//  ListConfig.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 31/05/2024.
//

import Foundation

protocol ListViewConfig {
    var numberOfShimmerCells: Int { get }
}
