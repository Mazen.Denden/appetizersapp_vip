//
//  Created by Mazen Denden on 03/11/2022.
//

import Foundation

protocol ListPresenter {
    func present(_ items: [AppetizerViewModel])
    func presentError()
    func presentLoader(_ isLoading: Bool)
    func presentDetails(for item: AppetizerViewModel)
}

class ListPresenterImplementation: ListPresenter {
    var viewController: ListViewControllerOutput?
    
    private let dispatchQueue: TestableDispatchQueue
    
    init(dispatchQueue: TestableDispatchQueue = DispatchQueue.main) {
        self.dispatchQueue = dispatchQueue
    }
    
    func present(_ items: [AppetizerViewModel]) {
        dispatchQueue.async {
            self.presentEmptyState(items.isEmpty)
            self.presentLoader(false)
            self.viewController?.display(items)
        }
    }
    
    func presentError() {
        dispatchQueue.async {
            self.presentLoader(false)
            self.viewController?.displayAlert()
        }
    }
    
    func presentLoader(_ isLoading: Bool) {
        dispatchQueue.async {
            self.viewController?.displayLoader(isLoading)
        }
    }
    
    func presentDetails(for item: AppetizerViewModel) {
        dispatchQueue.async {
            self.viewController?.displayDetails(for: item)
        }
    }
    
    func presentEmptyState(_ isEmpty: Bool) {
        dispatchQueue.async {
            self.viewController?.displayEmptyState(isEmpty)
        }
    }
    
}
