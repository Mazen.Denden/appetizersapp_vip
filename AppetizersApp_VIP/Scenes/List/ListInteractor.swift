//
//  Created by Mazen Denden on 03/11/2022.
//

protocol ListInteractor {
    func fetchAppetizers()
    func didSelectItem(at index: Int)
}

class ListInteractorImplementation: ListInteractor {
    var presenter: ListPresenter?
    var listWorker: ListWorker?
    var listStorageWorker: ListStorageWorker?
    var dataStore: ListDataStore?
    
    func fetchAppetizers() {
        Log.info("Retrieving data from API")
        
        presenter?.presentLoader(true)
        
        listWorker?.fetchAppetizers { [weak self] result in
            guard let self else { return }
            
            switch result {
            case .success(let appetizers):
                self.dataStore?.appetizers = appetizers
                self.presenter?.present(
                    appetizers.map {
                        AppetizerViewModel(appetizer: $0, isFavorite: self.appetizerIsFavorite(with: $0.name))
                    }
                )
                
                Log.info("Successfully retrieved data from API")
                
            case .failure(let error):
                self.presenter?.presentError()
                
                Log.error("Failed to retrieve data from API with error: \(error)")
            }
        }
    }
    
    func didSelectItem(at index: Int) {
        guard let appetizer = dataStore?.getAppetizer(at: index)
        else {
            return
        }
        
        presenter?.presentDetails(
            for: AppetizerViewModel(appetizer: appetizer, isFavorite: appetizerIsFavorite(with: appetizer.name))
        )
    }
    
    private func appetizerIsFavorite(with identifier: String) -> Bool {
        listStorageWorker?.isFavorite(with: identifier) ?? false
    }
}
