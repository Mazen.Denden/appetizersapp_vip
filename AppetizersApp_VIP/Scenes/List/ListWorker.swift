//
//  Created by Mazen Denden on 03/11/2022.
//

protocol ListWorker {
    func fetchAppetizers(completion: @escaping (ListServiceResult) -> Void)
}

class ListWorkerImplementation: ListWorker {
    private let listService: ListService
    
    init(listService: ListService) {
        self.listService = listService
    }
    
    func fetchAppetizers(completion: @escaping (ListServiceResult) -> Void) {
        listService.fetchAppetizers(completion: completion)
    }
}
