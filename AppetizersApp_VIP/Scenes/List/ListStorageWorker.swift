//
//  ListStorageWorker.swift
//  AppetizersApp_VIP
//
//  Created by Mazen Denden on 31/05/2024.
//

class ListStorageWorker {
    private let localStorage: ContainsFavoritesLocalStorage
    
    init(localStorage: ContainsFavoritesLocalStorage) {
        self.localStorage = localStorage
    }
    
    func isFavorite(with identifier: String) -> Bool {
        return localStorage.contains(identifier)
    }
}
