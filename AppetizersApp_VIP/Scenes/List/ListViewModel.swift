//
//  Created by Mazen Denden on 03/11/2022.
//

import UIKit

class ListViewModel: ListViewProtocol {
    
    var config: ListConfig?
    var isLoading: Bool?
    
    private let dataStore: ListDataStore
    private let localStorage: ContainsFavoritesLocalStorage
    
    init(dataStore: ListDataStore, localStorage: ContainsFavoritesLocalStorage) {
        self.dataStore = dataStore
        self.localStorage = localStorage
    }
    
    var title: String {
        return "🍟 Appetizers"
    }
    
    var backButtonTitle: String {
        return ""
    }
    
    var numberOfRows: Int {
        isLoading ?? false
            ? config?.numberOfShimmerCells ?? 0
            : dataStore.appetizers.count
    }
    
    var appetizers: [AppetizerViewModel] {
        return dataStore.appetizers
            .map {
                AppetizerViewModel(appetizer: $0, isFavorite: localStorage.contains($0.name))
            }
    }
    
    func appetizer(at index: Int) -> AppetizerViewModel? {
        guard let appetizer = dataStore.getAppetizer(at: index)
        else {
            return nil
        }
        
        return AppetizerViewModel(
            appetizer: appetizer,
            isFavorite: localStorage.contains(appetizer.name))
    }
    
    var emptyStateConfig: UIContentUnavailableConfiguration {
        var config = UIContentUnavailableConfiguration.empty()
        config.text = "No Appetizers"
        config.secondaryText = "Your Appetizers will appear here."
        return config
    }
    
}
