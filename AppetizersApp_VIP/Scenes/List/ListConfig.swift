//
//  Created by Mazen Denden on 31/05/2024.
//

struct ListConfig: ListViewConfig {
    let numberOfShimmerCells: Int
    
    init(numberOfShimmerCells: Int = 8) {
        self.numberOfShimmerCells = numberOfShimmerCells
    }
}
