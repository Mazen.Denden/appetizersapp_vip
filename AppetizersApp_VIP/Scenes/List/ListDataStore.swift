//
//  Created by Mazen Denden on 22/05/2024.
//

class ListDataStore {
    var appetizers: [Appetizer]
    
    init(appetizers: [Appetizer] = []) {
        self.appetizers = appetizers
    }
    
    func getAppetizer(at index: Int) -> Appetizer? {
        guard !appetizers.isEmpty,
              index >= 0,
              index < appetizers.count 
        else {
            return nil
        }
        
        return appetizers[index]
    }
}
