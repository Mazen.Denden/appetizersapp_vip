//
//  Created by Mazen Denden on 31/05/2024.
//

import UIKit

class NavigationControllerRouter: NavigationRouter {
    
    struct RouterError: Error, Equatable {
    }
    
    var navigationByType: [(TabType, UINavigationController)] = []
    
    func createNavigation(for tab: TabType, with navigationController: UINavigationController) {
        setupUI(navigationController, for: tab)
        append(navigationController, to: tab)
    }
    
    func show(_ viewController: UIViewController, on tab: TabType) {
        do {
            let navigationController = try navigationController(for: tab)
            navigationController.show(viewController, sender: navigationController.topViewController)
        } catch {
            Log.error("Could not get navigationController for tab: \(tab)")
        }
    }
    
    private func setupUI(_ navigationController: UINavigationController, for tab: TabType) {
        switch tab {
        case .home:
            navigationController.navigationBar.prefersLargeTitles = true
            navigationController.navigationBar.tintColor = .white
        case .favorites:
            navigationController.navigationBar.prefersLargeTitles = false
        }
    }
    
    private func append(_ navigationController: UINavigationController, to tab: TabType) {
        navigationByType.append((tab, navigationController))
    }
    
    func navigationController(for tab: TabType) throws -> UINavigationController {
        guard let (_, navigationController) = navigationByType.first(where: { $0.0 == tab })
        else {
            throw RouterError()
        }
        return navigationController
    }
}

