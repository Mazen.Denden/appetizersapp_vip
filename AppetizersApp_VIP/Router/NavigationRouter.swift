//
//  Created by Mazen Denden on 31/05/2024.
//

import UIKit

protocol NavigationRouter {
    func createNavigation(for tab: TabType, with navigationController: UINavigationController)
    func show(_ viewController: UIViewController, on tab: TabType)
}
