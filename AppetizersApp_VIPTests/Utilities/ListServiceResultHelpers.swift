//
//  ListServiceResultHelpers.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 04/06/2024.
//

import Foundation
@testable import AppetizersApp_VIP

extension ListServiceResult: Equatable {
    
    ///
    /// Add more use cases when needed
    public static func == (
        lhs: AppetizersApp_VIP.ListServiceResult,
        rhs: AppetizersApp_VIP.ListServiceResult
    ) -> Bool {
        switch (lhs, rhs) {
        case (.success, .success):
            return true
        case (.failure(let lhsError as ListServiceResponseError), .failure(let rhsError as ListServiceResponseError)):
            return lhsError == rhsError
        case (.failure(let lhsError as ListServiceDecodingError), .failure(let rhsError as ListServiceDecodingError)):
            return lhsError == rhsError
        case (.failure(let lhsError as CustomError), .failure(let rhsError as CustomError)):
            return lhsError == rhsError
        default:
            return false
        }
    }
    
}
