//
//  Created by Mazen Denden on 04/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

func assert<T>(
    _ expression: @autoclosure () throws -> T,
    throws expectedError: ListServiceResponseError,
    file: StaticString = #filePath,
    line: UInt = #line
) {
    AssertThrowsError(
        try expression(),
        throws: expectedError,
        file: file,
        line: line
    )
}

func assert<T>(
    _ expression: @escaping @autoclosure () throws -> T,
    throws expectedError: CustomError,
    file: StaticString = #filePath,
    line: UInt = #line
) {
    AssertThrowsError(
        try expression(),
        throws: expectedError,
        file: file,
        line: line
    )
}

func AssertThrowsError<T, E: Error & Equatable>(
    _ expression: @autoclosure () throws -> T,
    throws expectedError: E,
    file: StaticString = #filePath,
    line: UInt = #line
) {
    XCTAssertThrowsError(try expression()) { error in
        XCTAssertEqual(
            error as? E,
            expectedError,
            file: file,
            line: line
        )
    }
}
