//
//  NavigationControllerRouterTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 10/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class NavigationControllerRouterTest: XCTestCase {
    
    func test_createNavigationController_for_home() {
        let sut = NavigationControllerRouter()
        let navigationController = UINavigationController(rootViewController: UIViewController())
        
        sut.createNavigation(for: .home, with: navigationController)
        XCTAssertEqual(sut.navigationByType.first?.0, .home)
        XCTAssertEqual(sut.navigationByType.first?.1, navigationController)
    }
    
    func test_showViewController_twice_in_home() {
        let tab = TabType.home
        let (sut, navigationController) = makeSUT(for: tab)
        
        let firstViewController = UIViewController()
        sut.show(firstViewController, on: tab)
        
        let secondViewController = UIViewController()
        sut.show(secondViewController, on: tab)
        
        XCTAssertEqual(navigationController.viewControllers.count, 3)
    }
    
    func test_showViewController_twice_in_favorites() {
        let tab = TabType.favorites
        let (sut, navigationController) = makeSUT(for: tab)
        
        let firstViewController = UIViewController()
        sut.show(firstViewController, on: tab)
        
        let secondViewController = UIViewController()
        sut.show(secondViewController, on: tab)
        
        XCTAssertEqual(navigationController.viewControllers.count, 3)
    }
    
    func test_navigationControllerFor_withNoNavigationController_throwsAnError() {
        let sut = NavigationControllerRouter()
        
        AssertThrowsError(
            try sut.navigationController(for: .home),
            throws: NavigationControllerRouter.RouterError())
    }
    
    func test_navigationBarValues_forHomeTab() {
        let sut = NavigationControllerRouter()
        let navigationController = UINavigationController()
                
        sut.createNavigation(for: .home, with: navigationController)
        
        XCTAssertEqual(navigationController.navigationBar.prefersLargeTitles, true)
        XCTAssertEqual(navigationController.navigationBar.tintColor, .white)
    }
    
    func test_navigationBarValues_forFavoritesTab() {
        let sut = NavigationControllerRouter()
        let navigationController = UINavigationController()
        
        sut.createNavigation(for: .favorites, with: navigationController)
        
        XCTAssertEqual(navigationController.navigationBar.prefersLargeTitles, false)
    }
    
    // MARK: - Helpers
    
    private func makeSUT(for tab: TabType) -> (NavigationControllerRouter, UINavigationController) {
        let sut = NavigationControllerRouter()
        
        sut.createNavigation(
            for: tab, 
            with: NonAnimatedNavigationController(rootViewController: UIViewController())
        )
        
        let (_, navigationController) = sut.navigationByType.first(where: { $0.0 == tab })!
        
        return (sut, navigationController)
    }
    
    class NonAnimatedNavigationController: UINavigationController {
        override func pushViewController(_ viewController: UIViewController, animated: Bool) {
            super.pushViewController(viewController, animated: false)
        }
    }

}
