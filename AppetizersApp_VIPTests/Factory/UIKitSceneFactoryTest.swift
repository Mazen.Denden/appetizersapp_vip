//
//  UIKitSceneFactoryTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 10/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class UIKitSceneFactoryTest: XCTestCase {
    
    func test_makeList_returnsAConfigured_ListViewController() {
        let viewController = makeListViewController()
        
        XCTAssertNotNil(viewController.listViewModel)
        XCTAssertNotNil(viewController.interactor)
        XCTAssertNotNil(viewController.imageDataSource)
    }
    
    func test_makeList_correctlyConfiguresASelectionCallback() {
        let viewController = makeListViewController()
        
        XCTAssertNotNil(viewController.selection)
    }
    
    func test_makeAppetizerDetails_returnAConfigured_AppetizerDetailsViewController() {
        let viewController = makeAppetizerDetailsViewController()
        
        XCTAssertNotNil(viewController.viewModel)
        XCTAssertNotNil(viewController.interactor)
    }
    
    func test_makeFavorites_returnAConfigured_ListViewController() {
        let viewController = makeFavorites()
        
        XCTAssertNotNil(viewController.listViewModel)
        XCTAssertNotNil(viewController.interactor)
        XCTAssertNotNil(viewController.imageDataSource)
    }
    
    // MARK: - Helpers
    
    private func makeListViewController() -> ListViewController {
        return UIKitSceneFactory().makeList { _ in } as! ListViewController
    }
    
    private func makeAppetizerDetailsViewController() -> AppetizerDetailsViewController {
        let viewModel = AppetizerViewModel(appetizer: .sampleAppetizer, isFavorite: false)
        return UIKitSceneFactory().makeAppetizerDetails(with: viewModel) as! AppetizerDetailsViewController
    }
    
    private func makeFavorites() -> ListViewController {
        return UIKitSceneFactory().makeList { _ in } as! ListViewController
    }

}
