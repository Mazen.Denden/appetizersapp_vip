//
//  ListLocalServiceTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 10/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListLocalServiceTest: XCTestCase {
    
    func test_service_hasLocalStorageService_inSuccessCase() {
        let localStorage = FetchLocalStorage()
        let sut = makeSUT(
            result: .success,
            localStorage: localStorage
        )
        
        XCTAssertNotNil(sut.localStorage)
        
        sut.fetchAppetizers { result in
            XCTAssertNotEqual(result, .failure)
        }
    }
    
    func test_service_doesNotHaveLocalStorageService_inFailureCase() {
        let sut = makeSUT(
            result: .failure
        )
        
        XCTAssertNil(sut.localStorage)
    }

    func test_fetchAppetizers_returnOnlyFavorites() {
        let localStorage = FetchLocalStorage()
        localStorage.favorites = [
            "a favorite",
            "an other favorite"
        ]
        
        let sut = makeSUT(
            result: .success(fetchResult()),
            localStorage: localStorage
        )
        
        sut.fetchAppetizers { result in
            if case .success(let favorites) = result {
                for appetizer in favorites {
                    XCTAssertTrue(localStorage.favorites.contains(appetizer.name),
                                  "Failed to retrieve \(appetizer.name)")
                }
            }
        }
    }
    
    func test_fetchAppetizers_returnError() {
        let sut = makeSUT(
            result: .failure
        )
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .failure)
        }
    }
    
    // MARK: - Helpers
    
    private func makeSUT(
        result: ListServiceResult, 
        localStorage: FetchFavoritesLocalStorage? = nil
    ) -> ListLocalService {
        
        let apiService = ListServiceSpy()
        apiService.result = result
        
        let sut = ListLocalService(
            apiService: apiService
        )
        
        sut.localStorage = localStorage
        
        return sut
    }
    
    private func fetchResult() -> [Appetizer] {
        return [
            .init(name: "an appetizer", price: 0.0, imageURL: "", description: ""),
            .init(name: "an other appetizer", price: 0.0, imageURL: "", description: ""),
            .init(name: "a favorite", price: 0.0, imageURL: "", description: "")
        ]
    }

}

class FetchLocalStorage: FetchFavoritesLocalStorage {
    var favorites = [String]()
    
    func appetizers() -> [String] {
        return favorites
    }
    
}
