//
//  Created by Mazen Denden on 04/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListAPIServiceTest: XCTestCase {
    
    func test_fetchAppetizers_calledTwice_completesTwice() {
        let (sut, networkService) = makeSUT()
        
        sut.fetchAppetizers { _ in }
        sut.fetchAppetizers { _ in }
        
        XCTAssertEqual(networkService.stubbedCompletion.count, 2)
    }
    
    func test_fetchAppetizers_completes_withSuccess() {
        let (sut, _) = makeSUT()
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .success([]))
        }
    }
    
    func test_fetchAppetizers_completes_withFailureThrows_customError() {
        let error = NetworkError()
        let (sut, _) = makeSUT(customError: .networkError(error))
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .failure(error: CustomError.networkError(error)))
        }
    }
    
    func test_fetchAppetizers_completes_withFailureThrows_noResponse() {
        let (sut, _) = makeSUT(responseError: .noResponse)
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .failure(error: ListServiceResponseError.noResponse))
        }
    }
    
    func test_fetchAppetizers_completes_withFailureThrows_invalidResponse() {
        let (sut, _) = makeSUT(responseError: .invalidResponse)
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .failure(error: ListServiceResponseError.invalidResponse))
        }
    }
    
    func test_fetchAppetizers_completes_withFailureThrows_noData() {
        let (sut, _) = makeSUT(responseError: .noData)
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .failure(error: ListServiceResponseError.noData))
        }
    }
    
    func test_fetchAppetizers_completes_withFailureThrows_decodingError() {
        let (sut, _) = makeSUT(decodingError: .decodingError)

        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .failure(error: ListServiceDecodingError.decodingError))
        }
    }
    
    // MARK: - Helpers
    
    private func makeSUT(
        customError: CustomError? = nil,
        responseError: ListServiceResponseError? = nil,
        decodingError: ListServiceDecodingError? = nil
    ) -> (ListAPIService, NetworkServiceSpy) {
        
        let networkService = NetworkServiceSpy()
        networkService.error = customError
        
        let apiResponseHandler = APIResponseServiceSpy()
        apiResponseHandler.error = responseError
        apiResponseHandler.data = Data()
        
        let decoder = DecoderSpy()
        decoder.decodingResponse = .init(appetizers: [])
        decoder.error = decodingError
        
        let sut = ListAPIService(
            networkService: networkService,
            responseService: apiResponseHandler,
            decoder: decoder
        )
        
        return (sut, networkService)
    }
    
}

class NetworkServiceSpy: NetworkService {
    var stubbedCompletion = [(Data?, URLResponse?, (any Error)?)]()
    var error: CustomError?
    
    func request(
        _ request: URLRequest,
        completionHandler: @escaping @Sendable (Data?, URLResponse?, (any Error)?) -> Void
    ) {
        stubbedCompletion += [(nil, nil, nil)]
        completionHandler(nil, nil, error)
    }
}

class APIResponseServiceSpy: ListAPIResponse {
    var error: ListServiceResponseError?
    var data: Data?
    
    func handleResponse(
        data: Data?, response: URLResponse?, error: (any Error)?
    ) throws -> Data {
        if let error {
            throw error
        }
        
        if let error = self.error {
            throw error
        }
        
        guard let data = self.data
        else {
            XCTFail("You need to provide a data")
            throw ListServiceResponseError.noData
        }
        
        return data
    }
}

class DecoderSpy: Decoder {
    var error: ListServiceDecodingError?
    var decodingResponse: AppetizerResponse?
    
    func decode<T: Decodable>(
        _ data: Data
    ) throws -> T {
        if let error {
            throw error
        }
        
        guard let decodingResponse
        else {
            XCTFail("You need to provide a response")
            throw ListServiceResponseError.noResponse
        }
        
        guard let decodingResponse = decodingResponse as? T
        else {
            XCTFail("response is not decodable")
            throw ListServiceDecodingError.decodingError
        }
        
        return decodingResponse
    }
}

struct NetworkError: Error {
}
