//
//  Created by Mazen Denden on 04/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

class ListAPIResponseServiceTest: XCTestCase {
    
    func test_handleResponse_withError_throws_networkError() throws {
        let sut = ListAPIResponseService()
        assert(
            try sut.handle(
                error: NetworkError()
            ),
            throws: CustomError.networkError(NetworkError())
        )
    }
    
    func test_handleResponse_withNoResponse_throws_noResponse() throws {
        let sut = ListAPIResponseService()
        assert(
            try sut.handle(
                response: nil
            ),
            throws: .noResponse
        )
    }
    
    func test_handleResponse_withWrongResponseCode_throws_invalidResponse() throws {
        let sut = ListAPIResponseService()
        assert(
            try sut.handle(
                response: self.mockHttpResponse(statusCode: 100)
            ),
            throws: .invalidResponse
        )
        
        assert(
            try sut.handle(
                response: self.mockHttpResponse(statusCode: 300)
            ),
            throws: .invalidResponse
        )
    }
    
    func test_handleResponse_withNoData_throws_noData() throws {
        let sut = ListAPIResponseService()
        assert(
            try sut.handle(
                data: nil,
                response: self.mockHttpResponse(statusCode: 200)
            ),
            throws: .noData
        )
    }
    
    // MARK: - Helpers
    
    private func mockHttpResponse(statusCode: Int = 100) -> HTTPURLResponse {
        HTTPURLResponse(url: URL(string: "https://google.com")!, statusCode: statusCode, httpVersion: nil, headerFields: nil)!
    }

}

private extension ListAPIResponseService {
    func handle(data: Data? = Data(), response: URLResponse? = nil, error: Error? = nil) throws {
        _ = try self.handleResponse(data: data, response: response, error: error)
    }
}
