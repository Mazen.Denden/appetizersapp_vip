//
//  ListAPIRouterTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 10/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListAPIRouterTest: XCTestCase {

    func test_values_forFetch() {
        let sut = ListAPIRouter(route: .fetch)
        
        XCTAssertEqual(sut.baseUrl, "https://seanallen-course-backend.herokuapp.com/swiftui-fundamentals")
        XCTAssertEqual(sut.url, URL(string: "https://seanallen-course-backend.herokuapp.com/swiftui-fundamentals/appetizers"))
        XCTAssertEqual(sut.method, .get)
        XCTAssertNil(sut.headers)
    }
    
    func test_urlRequest_forFetch() {
        let sut = ListAPIRouter(route: .fetch)
        let url = URL(string: "https://seanallen-course-backend.herokuapp.com/swiftui-fundamentals/appetizers")!
        
        var expectation = URLRequest(url: url)
        expectation.cachePolicy = .returnCacheDataElseLoad
        
        XCTAssertEqual(sut.urlRequest, expectation)
    }
    
}
