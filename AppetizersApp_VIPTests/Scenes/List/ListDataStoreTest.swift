//
//  ListDataStoreTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 03/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListDataStoreTest: XCTestCase {
    
    func test_getAppetizer_atTheFirstIndex() {
        let mockAppetizers = [
            Appetizer.makeAppetizer()
        ]
        let sut = ListDataStore(appetizers: mockAppetizers)
        
        let appetizer = sut.getAppetizer(at: 0)
        
        XCTAssertNotNil(appetizer)
    }

}
