//
//  ListWorkerTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 03/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListWorkerTest: XCTestCase {

    func test_fetchAppetizers_completesWithSuccess() {
        let sut = makeSUT(result: .success)
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .success)
        }
    }
    
    func test_fetchAppetizers_completesWithFailure() {
        let sut = makeSUT(result: .failure)
        
        sut.fetchAppetizers { result in
            XCTAssertEqual(result, .failure)
        }
    }
    
    // MARK: - Helpers
    
    private func makeSUT(result: ListServiceResult) -> ListWorkerImplementation {
        let apiService = ListServiceSpy()
        apiService.result = result
        return ListWorkerImplementation(listService: apiService)
    }

}

class ListServiceSpy: ListService {
    var result: ListServiceResult!
    
    func fetchAppetizers(completion: @escaping (ListServiceResult) -> Void) {
        completion(result)
    }
    
}
