//
//  ListInteractorTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 03/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListInteractorTest: XCTestCase {
    
    func test_fetchAppetizers_completesWithSuccess_notifiesPresenter_presentItems() {
        let (sut, presenter, _) = makeSUT()
        sut.listWorker = makeWorker(with: .success)
        
        sut.fetchAppetizers()
        
        XCTAssertTrue(presenter.didCallPresentItems)
    }
    
    func test_fetchAppetizers_completesWithSuccess_mapsToViewModels() {
        let (sut, presenter, _) = makeSUT()
        let mockAppetizers = [
            Appetizer.makeAppetizer(name: "a name"),
            Appetizer.makeAppetizer(name: "another name")
        ]
        sut.listWorker = makeWorker(with: .success(mockAppetizers))
        
        let localStorage = ContainsLocalStorageSpy()
        localStorage.contains = true
        sut.listStorageWorker = ListStorageWorker(localStorage: localStorage)
        
        sut.fetchAppetizers()
        
        XCTAssertTrue(presenter.didCallPresentItems)
        XCTAssertEqual(presenter.presentedItems?.first?.name, "a name")
        XCTAssertEqual(presenter.presentedItems?.first?.isFavorite, true)
    }
    
    func test_fetchAppetizers_completesWithFailure_notifiesPresenter_presentError() {
        let (sut, presenter, _) = makeSUT()
        sut.listWorker = makeWorker(with: .failure)
        
        sut.fetchAppetizers()
        
        XCTAssertTrue(presenter.didCallPresentError)
    }
    
    func test_fetchAppetizers_completesWithSuccess_updatedDataStore() {
        let (sut, _, dataStore) = makeSUT()
        let mockAppetizers = [
            Appetizer.makeAppetizer(name: "a name"),
            Appetizer.makeAppetizer(name: "another name")
        ]
        
        sut.listWorker = makeWorker(with: .success(mockAppetizers))
        sut.fetchAppetizers()
        
        XCTAssertEqual(dataStore.appetizers.count, mockAppetizers.count)
        XCTAssertEqual(dataStore.appetizers.first?.name, "a name")
        XCTAssertEqual(dataStore.appetizers.last?.name, "another name")
    }
    
    func test_didSelectItem_notifierPresenter_presentDetails() {
        let (sut, presenter, dataStore) = makeSUT()
        dataStore.appetizers = [
            Appetizer.makeAppetizer(name: "a name")
        ]
        
        sut.didSelectItem(at: 0)
        
        XCTAssertTrue(presenter.didCallPresentDetails)
        XCTAssertEqual(dataStore.appetizers.first?.name, "a name")
    }
    
    func test_fetchAppetizers_notifierPresenter_presentLoader() {
        let (sut, presenter, _) = makeSUT()

        sut.fetchAppetizers()
        
        XCTAssertTrue(presenter.didCallPresentLoader)
    }
    
    // MARK: - Helpers
    
    private func makeSUT() -> (ListInteractorImplementation, ListPresenterSpy, ListDataStore) {
        let sut = ListInteractorImplementation()
        
        let presenter = ListPresenterSpy()
        sut.presenter = presenter
        
        let dataStore = ListDataStore()
        sut.dataStore = dataStore
        
        return (sut, presenter, dataStore)
    }
    
    private func makeWorker(with result: ListServiceResult) -> ListWorkerStub {
        let worker = ListWorkerStub()
        worker.result = result
        return worker
    }
    
}

extension Appetizer {
    static func makeAppetizer(name: String = "") -> Appetizer {
        Appetizer(name: name, price: 0, imageURL: "", description: "")
    }
}

private class ListWorkerStub: ListWorker {
    var result: ListServiceResult!
    
    func fetchAppetizers(completion: @escaping (ListServiceResult) -> Void) {
        completion(result)
    }
}

private class ListPresenterSpy: ListPresenter {
    
    var didCallPresentItems = false
    var presentedItems: [AppetizerViewModel]?
    
    var didCallPresentLoader = false
    
    var didCallPresentError = false
        
    var didCallPresentDetails = false
    
    func present(_ items: [AppetizerViewModel]) {
        didCallPresentItems = true
        presentedItems = items
    }
    
    func presentLoader(_ isLoading: Bool) {
        didCallPresentLoader = true
    }
    
    func presentError() {
        didCallPresentError = true
    }
    
    func presentDetails(for item: AppetizerViewModel) {
        didCallPresentDetails = true
    }
    
}

class ContainsLocalStorageSpy: ContainsFavoritesLocalStorage {
    var contains = false
    
    func contains(_ identifier: String) -> Bool {
        return contains
    }
    
}

extension ListServiceResult {
    static var success: ListServiceResult {
        return Self.success([])
    }
    
    static var failure: ListServiceResult {
        return Self.failure(error: ListServiceResponseError.noResponse)
    }
}
