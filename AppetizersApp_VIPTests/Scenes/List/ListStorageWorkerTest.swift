//
//  ListStorageWorkerTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 03/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListStorageWorkerTest: XCTestCase {

    func test_isFavorite_returnsTrue_ifItExists() {
        XCTAssertTrue(makeSUT(exists: true).isFavorite)
    }
    
    func test_isFavorite_returnsFalse_ifItDoestNotExist() {
        XCTAssertFalse(makeSUT(exists: false).isFavorite)
    }
    
    // MARK: - Helpers
    
    private func makeSUT(exists: Bool) -> ListStorageWorker {
        let localStorage = ContainsFavoritesLocalStorageSpy()
        localStorage.exists = exists
        return ListStorageWorker(localStorage: localStorage)
    }

}

private extension ListStorageWorker {
    var isFavorite: Bool {
        self.isFavorite(with: "")
    }
}

class ContainsFavoritesLocalStorageSpy: ContainsFavoritesLocalStorage {
    var exists = false
    
    func contains(_ identifier: String) -> Bool {
        exists
    }
    
}
