//
//  ListPresenterTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 03/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListPresenterTest: XCTestCase {
    
    func test_presentItems_notifies_displayItemsOfViewController() {
        let sut = makeSUT()
        
        let viewController = ListViewControllerSpy()
        sut.viewController = viewController
        
        sut.present([])
        
        XCTAssertTrue(viewController.didCallDisplayAppetizers)
    }
    
    func test_presentEmptyItems_notifiesViewController_withNoItems() {
        let sut = makeSUT()
        
        let viewController = ListViewControllerSpy()
        sut.viewController = viewController
        
        sut.present([])
        
        XCTAssertEqual(viewController.appetizersToDisplay!.count, 0)
    }
    
    func test_presentItems_notifiesViewController_withCorrectNumberOfItems() {
        let sut = makeSUT()
        
        let viewController = ListViewControllerSpy()
        sut.viewController = viewController
        
        sut.present([
            mock,
            mock,
            mock
        ])
        
        XCTAssertEqual(viewController.appetizersToDisplay!.count, 3)
    }
    
    func test_presentError_notifies_displayAlertOfViewController() {
        let sut = makeSUT()
        
        let viewController = ListViewControllerSpy()
        sut.viewController = viewController
        
        sut.presentError()
        
        XCTAssertTrue(viewController.didCallDisplayAlert)
    }
    
    func test_presentLoader_notifies_displayLoaderOfViewController() {
        let sut = makeSUT()
        
        let viewController = ListViewControllerSpy()
        sut.viewController = viewController
        
        sut.presentLoader(false)
        XCTAssertFalse(viewController.isLoading!)
        
        sut.presentLoader(true)
        XCTAssertTrue(viewController.isLoading!)
        
        XCTAssertTrue(viewController.didCallDisplayLoader)
    }
    
    func test_presentDetails_notifies_displayDetailsOfViewController() {
        let sut = makeSUT()
        
        let viewController = ListViewControllerSpy()
        sut.viewController = viewController
        
        sut.presentDetails(for: mock)
        
        XCTAssertTrue(viewController.didCallDisplayDetails)
        XCTAssertNotNil(viewController.item)
        XCTAssertEqual(viewController.item?.identifier, mock.identifier)
    }
    
    func test_presentDetails_notifies_displayEmptyStateOfViewController() {
        let sut = makeSUT()
        let viewController = ListViewControllerSpy()
        sut.viewController = viewController
        
        sut.presentEmptyState(false)
        
        XCTAssertTrue(viewController.didCallDisplayEmptyState)
    }
    
    // MARK: - Helpers
    
    private func makeSUT() -> ListPresenterImplementation {
        return ListPresenterImplementation(dispatchQueue: DispatchQueueMock())
    }
    
    private var mock: AppetizerViewModel {
        AppetizerViewModel(appetizer: .sampleAppetizer, isFavorite: false)
    }

}

private class ListViewControllerSpy: ListViewControllerOutput {
    var didCallDisplayAppetizers = false
    var appetizersToDisplay: [AppetizerViewModel]?
    
    var didCallDisplayAlert = false
    
    var didCallDisplayLoader = false
    var isLoading: Bool?
    
    var didCallDisplayDetails = false
    var item: AppetizerViewModel?
    
    var didCallDisplayEmptyState = false
    
    func display(_ appetizers: [AppetizerViewModel]) {
        didCallDisplayAppetizers = true
        appetizersToDisplay = appetizers
    }
    
    func displayAlert() {
        didCallDisplayAlert = true
    }
    
    func displayLoader(_ isLoading: Bool) {
        didCallDisplayLoader = true
        self.isLoading = isLoading
    }
    
    func displayDetails(for item: AppetizerViewModel) {
        didCallDisplayDetails = true
        self.item = item
    }
    
    func displayEmptyState(_ isEmpty: Bool) {
        didCallDisplayEmptyState = true
    }
    
}

final class DispatchQueueMock: TestableDispatchQueue {
    func async(execute work: @escaping @convention(block) () -> Void) {
        work()
    }
}
