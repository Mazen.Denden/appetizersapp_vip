//
//  Created by Mazen Denden on 03/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListViewModelTest: XCTestCase {

    func test_attributes() {
        let (sut, _, _) = makeSUT()
        XCTAssertEqual(sut.title, "🍟 Appetizers")
        XCTAssertEqual(sut.backButtonTitle, "")
    }
    
    func test_numberOfRows_whenLoading() {
        let (sut, _, _) = makeSUT()
        let config = ListConfig(numberOfShimmerCells: 5)
        
        sut.config = config
        sut.isLoading = true
        
        XCTAssertEqual(sut.numberOfRows, config.numberOfShimmerCells)
    }
    
    func test_numberOfRows_whenNotLoading() {
        let (sut, _, _) = makeSUT()
        
        sut.isLoading = false
        
        XCTAssertEqual(sut.numberOfRows, sut.appetizers.count)
    }
    
    func test_appetizerIsFavorite_ifItExistsInLocalStorage() {
        let mockAppetizers: [Appetizer] = [
            .makeAppetizer()
        ]
        let (sut, _, localStorage) = makeSUT(appetizers: mockAppetizers, exists: true)
        
        let appetizerViewModel = sut.appetizer(at: 0)
        
        XCTAssertEqual(appetizerViewModel?.isFavorite, localStorage.exists)
    }
    
    func test_appetizers_returnsAppetizersFromDataStore() {
        let mockAppetizers: [Appetizer] = [
            .makeAppetizer(),
            .makeAppetizer(),
            .makeAppetizer()
        ]
        let (sut, dataStore, _) = makeSUT(appetizers: mockAppetizers)
        
        XCTAssertEqual(sut.appetizers.count, dataStore.appetizers.count)
    }
    
    // MARK: - Helpers
    
    private func makeSUT(appetizers: [Appetizer] = [.makeAppetizer()], exists: Bool = false) -> (ListViewModel, ListDataStore, ContainsFavoritesLocalStorageSpy) {
        let dataStore = ListDataStore(appetizers: appetizers)
        let localStorage = ContainsFavoritesLocalStorageSpy()
        localStorage.exists = exists
        
        let sut = ListViewModel(dataStore: dataStore, localStorage: localStorage)
        return (sut, dataStore, localStorage)
    }

}
