//
//  ListViewControllerTest.swift
//  AppetizersApp_VIPTests
//
//  Created by Mazen Denden on 03/06/2024.
//

import XCTest
@testable import AppetizersApp_VIP

final class ListViewControllerTest: XCTestCase {
    
    private let storyboardIdentifier = "List"
    
    func test_instantiate_fromStoryboard() {
        let sut = ListViewController.instantiateInitial(from: storyboardIdentifier)
        XCTAssertNotNil(sut)
    }
    
    func test_instantiate_withListViewModel() {
        XCTAssertNotNil(makeSUT().listViewModel)
    }
    
    func test_viewDidLoad_setsTheNavigationItems() {
        let sut = makeSUT()
        
        XCTAssertEqual(sut.navigationItem.title, "a title")
        XCTAssertEqual(sut.navigationItem.backButtonTitle, "a button title")
    }
    
    func test_tableView_NotNil() {
        XCTAssertNotNil(makeSUT().tableView)
    }
    
    func test_viewDidLoad_registersAppetizerCell() {
        let sut = makeSUT()
        let tableView = sut.tableView
        
        let nib = tableView?.dequeueReusableCell(withIdentifier: "AppetizerCell")
        
        XCTAssertNotNil(nib)
    }
    
    func test_tableView_showsCorrectNumberOfCells() {
        let sut = makeSUT()
        
        let numberOfCells = sut.tableView?.numberOfRows(inSection: 0)
        XCTAssertEqual(numberOfCells, sut.listViewModel.numberOfRows)
    }
    
    func test_tableView_showsCorrectCells() {
        let cell = makeSUT().tableView?.cell(at: 0) as? AppetizerCell
        XCTAssertNotNil(cell)
    }
    
    func test_selectRow_notifiesInteractor() {
        let interactor = ListInteractorSpy()
        let sut = makeSUT()
        sut.interactor = interactor
        
        sut.tableView?.select(row: 0)
        
        XCTAssertTrue(interactor.didCallSelectItem)
    }
    
    // MARK: Helpers
    
    private func makeSUT() -> ListViewController {
        let sut: ListViewController = ListViewController.instantiateInitial(from: storyboardIdentifier)
        sut.listViewModel = ViewModelStub()
        sut.loadViewIfNeeded()
        return sut
    }
    
}

private extension UITableView {
    func cell(at row: Int) -> UITableViewCell? {
        return dataSource?.tableView(self, cellForRowAt: IndexPath(row: row, section: 0))
    }
    
    func select(row: Int) {
        let indexPath = IndexPath(row: row, section: 0)
        selectRow(at: indexPath, animated: false, scrollPosition: .none)
        delegate?.tableView?(self, didSelectRowAt: indexPath)
    }
}

private class ListInteractorSpy: ListInteractor {
    var didCallFetchAppetizers: Bool = false
    var didCallSelectItem: Bool = false
    
    func fetchAppetizers() {
        didCallFetchAppetizers = true
    }
    
    func didSelectItem(at index: Int) {
        didCallSelectItem = true
    }
    
}

private class ViewModelStub: ListViewProtocol {
    var emptyStateConfig: UIContentUnavailableConfiguration {
        .empty()
    }
    
    var title: String {
        return "a title"
    }
    
    var backButtonTitle: String {
        return "a button title"
    }
    
    var isLoading: Bool?
    
    var numberOfRows: Int {
        return 1
    }
    
    func appetizer(at index: Int) -> AppetizersApp_VIP.AppetizerViewModel? {
        return nil
    }
    
}
